<?php
// use \Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*--------------------------------------------------------------------------------------------------------*/
/*-------------------------Routes for different pages of Home-----------------------------------------*/
/*--------------------------------------------------------------------------------------------------------*/
Route::get('/', ['uses' => 'Home\HomeController@getIndex', 'as' => 'get.home' ]);



/*--------------------------------------------------------------------------------------------------------*/
/*-------------------------Routes for different pages of property-----------------------------------------*/
/*--------------------------------------------------------------------------------------------------------*/
// Route::get('/hotel', ['uses' => 'Property\PropertyController@getIndex', 'as' => 'get.property' ]);

Route::get('/add-hotel', ['uses' => 'Hotel\HotelController@addHotel', 'as' => 'get.new-home' ]);
Route::post('/add-hotel', ['uses' => 'Hotel\HotelController@postAddHotel', 'as' => 'post.new-home']);


Route::prefix('admin')->group(function(){
    Route::get('login', ['uses' => 'Auth\LoginController@getLogin', 'as' => 'login']);
    Route::post('login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'post.login']);
    Route::get('logout', ['uses' => 'Auth\LoginController@getLogout', 'as' => 'get.logout']);

    Route::get('dashboard', ['uses' => 'backend\DashboardController@getIndex', 'as' => 'dashboard']);
});


// Route::get('user/home', ['uses' => 'backend\DashboardController@getIndex', 'as' => 'user.dashboard']);

?>
