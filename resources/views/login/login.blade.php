@extends('_shared.layouts.master')
@section('main-content')
<!-- Login Screen -->


<div class="login-container">
  <div class="login-logo text-center">
    <img class="main-logo" src="{{ asset('public/assets/images/bookinglogo1.jpg') }}" alt="logo">
  </div>
  <div class="login-wrapper text-center">
    <span class="login-page-sub-title login-text">&mdash; Log In &mdash;</span>
    @if(Session::get('loggedOut'))
    <p class="logged-out-msg alert alert-info"><i class="fa fa-check"></i> You have been successfully logged out!</p>
    @endif
    <form action="" method="post" class="login-form mt-2 mb-2">
      {!! csrf_field() !!}
      @if($errors->has('input-email') || $errors->has('input-password'))
      <label class="alert alert-danger" style="background-color: #fff;"><i class="fa fa-exclamation-triangle"></i> Email and password fields are required!</label>
      @endif
      @if(Session::get('loginError'))
      <label class="alert alert-danger" style="background-color: #fff;"><i class="fa fa-exclamation-triangle"></i> Invalid Username or Password!</label>
      @endif
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-addon">
            <i class="fa fa-envelope"></i>
          </span>
          <input class="form-control" placeholder="Enter Email" type="text" name="input-email">
        </div>
      </div>
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-addon">
            <i class="fa fa-lock"></i>
          </span>
          <input class="form-control" placeholder="Password" type="password" name="input-password">
        </div>
      </div>
      <a class="pull-right link-text" href="#">Forgot password?</a>
      <div class="text-left">
        <label class="check-container"><span class="login-text">Keep me logged in</span>
          <input type="checkbox" name="remember-me">
          <span class="checkmark"></span>
        </label>
      </div>
      <input class="btn btn-lg btn-primary btn-block login-submit" type="submit" value="Log in">
    </form>
    <span class="login-text">Don't have an account yet?</span>&nbsp;&nbsp;<a class="link-text" href="#">Sign up now</a>
  </div>
  <!-- End Login Screen -->
</div>
@stop
