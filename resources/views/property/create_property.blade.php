<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Create property</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#property-contact">Property Contact</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#property">Property</a>
    </li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div id="property-contact" class="container tab-pane active">
    <form action="" method="post">
      <div class="form-row">
    <div class="form-group col-md-6">
      <label>Company</label>
      <input type="text" class="form-control" name="input-company">
    </div>
    <div class="form-group col-md-6">
      <label>First Name</label>
      <input type="text" class="form-control" name="input-fname">
    </div>
    <div class="form-group col-md-6">
      <label>Middle Name</label>
      <input type="text" class="form-control" name="input-mname">
    </div>
    <div class="form-group col-md-6">
      <label>Last Name</label>
      <input type="text" class="form-control" name="input-lname">
    </div>
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="text" class="form-control" name="input-email">
    </div>
    <div class="form-group col-md-6">
      <label>Mobile Number</label>
      <input type="text" class="form-control" name="input-mob">
    </div>
    <div class="form-group col-md-6">
      <label>Phone Region Code</label>
      <input type="text" class="form-control" name="input-phone-code">
    </div>
    </div>
  </div><!--form-row ends-->

    <div id="property" class="container tab-pane fade"><br>
      <div class="form-row">
        <div class="form-group col-md-6">
      <label>Select Property Contact</label>
      <select class="custom-select">
        <option selected>Open this select menu</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label>Select property type</label>
      <select class="custom-select">
        <option selected>Open this select menu</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>

    <div class="form-group col-md-6">
      <label for="inputName">Enter name</label>
      <input type="text" class="form-control" id="inputName" placeholder="Name" name="input-name">
    </div>
    <div class="form-group col-md-6">
      <label for="inputLocation">Enter Location</label>
      <input type="text" class="form-control" id="inputLocation" placeholder="Location" name="input-location">
    </div>

    <div class="form-group col-md-6">
      <label>Select Country</label>
      <select class="custom-select">
        <option selected>Open this select menu</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>
  <div class="form-group col-md-6">
    <label for="inputAddress">Address Line</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="input-address">
  </div>
</div>
  <div class="form-group">
    <label for="inputCity">City</label>
    <select id="inputCity" class="form-control" name="input=city">
        <option selected>Select City</option>
        <option>...</option>
      </select>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputPostalCode">Postal Code</label>
      <input type="text" class="form-control" id="inputPostalCode" name="input-postal">
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">State</label>
      <input type="text" class="form-control" id="inputState" placeholder="Apartment, studio, or floor" name="input-state">
    </div>
    <div class="form-group col-md-2">
      <label for="inputUrl">Website Url</label>
      <input type="text" class="form-control" id="inputUrl" name="input-url">
    </div>
  </div>
  <div class="form-group">
      <label for="inputRooms">No of Rooms.</label>
      <input class="form-control" type="text" id="inputRooms" name="input-rooms">
  </div>

    <div class="form-group">
      <label>Currency</label>
      <select class="custom-select" name="input-currency">
        <option selected>Open this select menu</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>
    <div class="form-group">
      <label for="inputImage">Image</label>
      <input class="form-control" type="file" id="inputImage" name="input-image">
    </div>
  </div>
      </div><!--form row ends-->
    </div>

  </div>
</form>
</div>


</body>
</html>