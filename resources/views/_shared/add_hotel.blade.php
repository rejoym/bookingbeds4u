@extends('_shared.layouts.master')
@section('main-content')
<?php
$currency_type = [
  'US$' => 'dollar',
  'NRS' => 'nrs',
  'INRS' => 'inrs'
];
?>
<section class="add-hotel">
  <div class="form-wrapper">
    <div class="row">
      <div class="col-md-12">
        <h2 class="register-title"><span class="register-main">Register</span> Your Hotel.</h2>
        @if(session(['Registered']))
          <div class="alert alert-success hotel-add-success" role="alert"><strong>Success! </strong>{{Session::get('Registered')}}</div>
        @endif
        <span class="reg-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam sunt, eius! Esse eaque accusamus commodi explicabo, placeat, mollitia. At, quidem!</span>
        <ul id="tabsJustified" class="nav nav-tabs register-tabs reg-tabs">
          <li class="nav-item"><a href="" data-target="#contact-info" data-toggle="tab" class="nav-link small text-uppercase active">User Information</a></li>
          <li class="nav-item"><a href="" data-target="#prop-info" data-toggle="tab" class="nav-link small text-uppercase">Property Info</a></li>
          <li class="nav-item"><a href="" data-target="#ext-info" data-toggle="tab" class="nav-link small text-uppercase">Ext. Info</a></li>
        </ul>
        <br>
        <form id="" class="add-hotel" method="post" action="" enctype="multipart/form-data">
          {!! csrf_field() !!}
        <div id="tabsJustifiedContent" class="tab-content">
          <div id="contact-info" class="tab-pane fade active show" role="tabpanel">
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('input-company') ? 'has-error' : ''}}">
                <label for="input-company">What is your company name?</label>
                <input type="text" class="form-control" id="input-company" placeholder="Company Name" name="input-company">
                <span class="form-error">{{ $errors->first('input-company') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4{{ $errors->has('input-fname') ? 'has-error' : ''}}">
                <label for="input-fname">First Name<NOFRAMES></NOFRAMES></label>
                <input type="text" class="form-control" id="input-fname" placeholder="First Name" name="input-fname">
                <span class="form-error">{{ $errors->first('input-fname') }}</span>
              </div>
              <div class="form-group col-md-4">
                <label for="input-mname">Middle Name</label>
                <input type="text" class="form-control" id="input-mname" placeholder="Middle Name" name="input-mname">
              </div>
              <div class="form-group col-md-4{{ $errors->has('input-lname') ? 'has-error' : ''}}">
                <label for="input-lname">Last Name</label>
                <input type="text" class="form-control" id="input-lname" placeholder="Last Name" name="input-lname">
                <span class="form-error">{{ $errors->first('input-lname') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('input-email') ? 'has-error' : ''}}">
                <label for="input-email">Your Email</label>
                <input type="text" class="form-control" id="input-email" placeholder="Enter Your Email" name="input-email">
                <span class="form-error">{{ $errors->first('input-email') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('input-phn-code') ? 'has-error' : ''}}">
                <label for="input-phn-code">Phone Country/Region Code</label>
                <input type="text" class="form-control" id="input-phn-code" placeholder="Phone Code" name="input-phn-code">
                <span class="form-error">{{ $errors->first('input-phn-code' )}}</span>
              </div>
              <div class="form-group col-md-6{{ $errors->has('input-phn-num') ? 'has-error' : ''}}">
                <label for="input-phn-num">Phone Number</label>
                <input type="text" class="form-control" id="input-phn-num" placeholder="Phone No." name="input-phn-num">
                <span class="form-error">{{ $errors->first('input-phn-num') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eum placeat eius eos voluptas vitae quis itaque eveniet debitis consectetur?</label>
              </div>
            </div>
          </div><!-- tab#home1 -->
          <div id="prop-info" class="tab-pane fade" role="tabpanel">
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('input-prop-name') ? 'has-error' : ''}}">
                <label for="input-prop-name">Property Name</label>
                <input type="text" class="form-control" id="input-prop-name" placeholder="Property Name" name="input-prop-name">
                <span class="form-error">{{ $errors->first('input-prop-name') }}</span>
              </div>
              <div class="form-group col-md-6{{ $errors->has('input-nos-rooms') ? 'has-error' : ''}}">
                <label for="input-nos-rooms">How Many Rooms?</label>
                <input type="text" class="form-control" id="input-nos-rooms" placeholder="Number of Rooms" name="input-nos-rooms">
                <span class="form-error">{{ $errors->first('input-nos-rooms') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('prop_type') ? 'has-error' : ''}}">
                <label for="input-prop-type">Type of Property</label>
                <select class="form-control" name="prop_type">
                  @foreach($data['all_prop_type'] as $types)
                  <option value="{{$types->id}}">{{$types->label}}</option>
                  @endforeach
                </select>
                <span class="form-error">{{ $errors->first('prop_type') }}</span>
              </div>
              <div class="form-group col-md-6{{ $errors->has('prop_country') ? 'has-error' : ''}}">
                <label for="input-country">Select Country</label>
                <select class="form-control" name="prop_country">
                  @foreach($data['all_country'] as $countries)
                  <option value="{{$countries->id}}">{{$countries->label}}</option>
                  @endforeach
                </select>
                <span class="form-error">{{ $errors->first('prop_country') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('input-location') ? 'has-error' : ''}}">
                <label for="input-location">Enter Location<NOFRAMES></NOFRAMES></label>
                <input type="text" class="form-control" id="input-location" placeholder="Enter Location" name="input-location">
                <span class="form-error">{{ $errors->first('input-locaiton') }}</span>
              </div>
              <div class="form-group col-md-6{{ $errors->has('input-add-line1') ? 'has-error' : ''}}">
                <label for="input-mname">Address Line1</label>
                <input type="text" class="form-control" id="input-add-line1" placeholder="Enter Address Line1" name="input-add-line1">
                <span class="form-error">{{ $errors->first('input-add-line1') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4{{ $errors->has('input-city') ? 'has-error' : ''}}">
                <label for="input-city">City</label>
                <input type="text" class="form-control" id="input-city" placeholder="City" name="input-city">
                <span class="form-error">{{ $errors->first('input-city') }}</span>
              </div>
              <div class="form-group col-md-4{{ $errors->has('input-postal-code') ? 'has-error' : ''}}">
                <label for="input-postal-code">Postal Code</label>
                <input type="text" class="form-control" id="input-postal-code" placeholder="Postal Code" name="input-postal-code">
                <span class="form-error">{{ $errors->first('input-postal-code') }}</span>
              </div>
              <div class="form-group col-md-4{{ $errors->has('input-state') ? 'has-error' : ''}}">
                <label for="input-state">State</label>
                <input type="text" class="form-control" id="input-state" placeholder="State" name="input-state">
                <span class="form-error">{{ $errors->first('input-state') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('input-website-url') ? 'has-error' : ''}}">
                <label for="input-website-url">Website URL</label>
                <input type="text" class="form-control" id="input-website-url" placeholder="Website URL" name="input-website-url">
                <span class="form-error">{{ $errors->first('input-website-url') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12{{ $errors->has('input-prop-desc') ? 'has-error' : ''}}">
                <label for="input-website-url">Property Description</label>
                <textarea class="form-control input-prop-description" placeholder="Write something about your property." name="input-prop-desc"></textarea>
                <span class="form-error">{{ $errors->first('input-prop-desc') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eum placeat eius eos voluptas vitae quis itaque eveniet debitis consectetur?</label>
              </div>
            </div>
          </div><!-- tab#prop-info -->
          <div id="ext-info" class="tab-pane fade">
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('currency') ? 'has-error' : ''}}">
                <label for="input-prop-name">Select Currency</label>
                <select class="form-control" name="currency">
                @foreach($currency_type as $key=>$value)
                  <option value="{{$value}}">{{$key}}</option>

                @endforeach
                </select>
                <span class="form-error">{{ $errors->first('currency') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6{{ $errors->has('display_img') ? 'has-error' : ''}}">
                <label for="input-display-img">Select Display Image for your property.</label>
                <input type="file" name="display_img" id="input-display-img">
                <span class="form-error">{{ $errors->first('display_image') }}</span>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eum placeat eius eos voluptas vitae quis itaque eveniet debitis consectetur?</label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">SUBMIT</button>
        </div>
        </form>
      </div>
    </div>
  </div>

</section>

@stop
