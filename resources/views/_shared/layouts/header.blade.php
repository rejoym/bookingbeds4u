<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Surjith S M">
  <meta name="description" content="#">
  <meta name="keywords" content="#">

  <script src="{{ asset('public/assets/cdn-cgi/apps/head/-mEFVS8y7qx5pVzWHQTCQu5gnVM.js') }}"></script><link rel="shortcut icon" href="#">

  <title>Booking beds4u</title>

  <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('public/assets/css/themify-icons.css') }}">

  <link rel="stylesheet" href="{{ asset('public/assets/css/font-awesome.min.css') }}">

  <link href="{{ asset('public/assets/css/set1.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('public/assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/css/custom.css') }}">
</head>
<body>
  <div class="nav-menu sticky-top">
    <div class="bg transition">
      <div class="container-fluid fixed">
        <div class="row">
          <div class="col-md-12">
            <nav class="navbar navbar-expand-lg">
              <a class="navbar-brand" href="index.html"><img class="main-logo" src="{{ asset('public/assets/images/bookinglogo1.jpg') }}" alt="logo"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
              </button>
              <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Hotels
                      <span class="ti-angle-down"></span>
                    </a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Kathmandu</a>
                      <a class="dropdown-item" href="#">Pokhara</a>
                      <a class="dropdown-item" href="#">Sauraha</a>
                      <a class="dropdown-item" href="#">Bhaktapur</a>
                      <a class="dropdown-item" href="#">Nagarkot</a>
                    </div>
                  </li>
                  <!-- <li class="nav-item active">
                    <a class="nav-link" href="#">Help</a>
                  </li> -->
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                  </li>

                  <!-- <li><a href="{{ url('/hotel') }} " class="btn btn-outline-danger top-btn"><span class="ti-plus"></span> Add Hotel</a></li> -->
                  <li><a href="{{ url('add-hotel') }}" class="btn btn-outline-danger top-btn"><span class="ti-plus"></span> Add Hotel</a></li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="login" data-toggle="pill" href="index.html#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">LOGIN</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="sign-up" data-toggle="pill" href="index.html#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">SIGN UP</a>
          </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="login">
            <div class="modal-header">
              <h5 class="modal-title"><img src="{{ asset('public/assets/images/logo.png') }}" alt="logo"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="ti-close"></span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <input type="text" class="form-control add-listing_form" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control add-listing_form" placeholder="Password">
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-center">
              <button type="button" class="btn btn-primary">LOG IN</button>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="sign-up">
            <div class="modal-header">
              <h5 class="modal-title"><img src="{{ asset('public/assets/images/logo.png') }}" alt="logo"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="ti-close"></span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <input type="text" class="form-control add-listing_form" id="name" placeholder="Your name">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control add-listing_form" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control add-listing_form" id="username" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control add-listing_form" id="password" placeholder="Password">
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-center">
              <button type="button" class="btn btn-primary">CREATE ACCOUNT</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> --}}
