<footer class="main-block gray">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 responsive-wrap">
        <div class="location">
        </div>
      </div>
      <div class="col-md-4 responsive-wrap">
        <div class="footer-logo_wrap">
          <!-- <img src="{{ asset('public/assets/images/footer-logo.png') }}" alt="#" class="img-fluid"> -->
        </div>
      </div>
      <div class="col-md-4 responsive-wrap">
        <ul class="social-icons">
          <li><a href="index.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="index.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="index.html#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-12">
        <div class="copyright">
          <p>Copyright © 2017 booking beds4u Inc. All rights reserved</p>
          <a href="#">Privacy</a>
          <a href="#">Terms</a>
        </div>
      </div>
    </div>
  </div>
</footer>



<script src="{{ asset('public/assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('public/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
</body>
</html>
