<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostHotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-company' => 'required',
            'input-fname' => 'required',
            'input-lname' => 'required',
            'input-email' => 'required|email',
            'input-phn-code' => 'required|numeric',
            'input-phn-num' => 'required|numeric',
            'prop_type' => 'required',
            'prop_country' => 'required',
            'input-prop-name' => 'required',
            'input-location' => 'required',
            'input-add-line1' => 'required',
            'input-city' => 'required',
            'input-postal-code' => 'required|numeric',
            'input-state' => 'required',
            'input-website-url' => 'required',
            'input-nos-rooms' => 'required|numeric',
            'input-prop-desc' => 'required',
            'currency' => 'required',
            'display_image' => 'required|image|max:1024|mimes:jpeg,png,jpg'
        ];
    }
}
