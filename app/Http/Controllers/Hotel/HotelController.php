<?php

namespace App\Http\Controllers\Hotel;

use \Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostHotelRequest;
use App\Models\Property_contact as Contact;
use App\Models\Property as Property;
use App\Models\Property_type as Type;
use App\Models\Country as Country;


class HotelController extends Controller
{
  public function index() {

  }

  /**
   * create porperty contact
   * @param  array  $data [the data to be stored]
   * @return [object]       [description]
   */
  public function createPropertyContact(array $data) {
    return Contact::create($data);
  }

  /**
   * function to insert property details into the database
   * @param  array  $data [data to be inserted]
   * @return [Object]       [description]
   */
  public function createProperty(array $data) {
    return Property::create($data);
  }

  /**
   * function to handle the get request
   * @return view
   */
  public function addHotel() {
    $data['all_prop_type'] = Type::all();
    $data['all_country'] = Country::all();
    return view('_shared.add_hotel', compact('data'));
  }

  /**
   * function to insert post data of the Hotel
   * @param  PostHotelRequest $request [description]
   * @return [type]                    [description]
   */
  public function postAddHotel(PostHotelRequest $request) {
    $company = $request->input('input-company');
    $first_name = $request->input('input-fname');
    $middle_name = $request->input('input-mname');
    $last_name = $request->input('input-lname');
    $email = $request->input('input-email');
    $ph_code = $request->input('input-phn-code');
    $ph_number = $request->input('input-phn-num');

    $data_contact = [
      'company' => $company,
      'first_name' => $first_name,
      'middle_name' => $middle_name,
      'last_name' => $last_name,
      'email' => $email,
      'mob_no' => $ph_number,
      'phone_region_code' => $ph_code
    ];

    $insert_contact = $this->createPropertyContact($data_contact);

    $contact_id = $insert_contact['id'];
    $prop_type_id = $request->input('prop_type');
    $country_id = $request->input('prop_country');
    $prop_name = $request->input('input-prop-name');
    $location = $request->input('input-location');
    $add_line1 = $request->input('input-add-line1');
    $city = $request->input('input-city');
    $postal_code = $request->input('input-postal-code');
    $state = $request->input('input-state');
    $website_url = $request->input('input-website-url');
    $no_rooms = $request->input('input-nos-rooms');
    $prop_desc = $request->input('input-prop-desc');
    $currency = $request->input('currency');
    // dd($request->file('display_img'));
    if($request->file('display_img') != '') {
            $image1_rand_name = str_random(10);
            $image1_name = $image1_rand_name.time().'.'.$request->file('display_img')->getClientOriginalExtension();
            $request->file('display_img')->move(
                base_path() . '/public/assets/images' .$image1_name
            );
    }

    $data_property = [
      'property_contact_id' => $contact_id,
      'property_type_id' => $prop_type_id,
      'country_id' => $country_id,
      'name' => $prop_name,
      'location' => $location,
      'address_line' => $add_line1,
      'city' => $city,
      'postal_code' => $postal_code,
      'state' => $state,
      'website_url' => $website_url,
      'no_of_rooms' => $no_rooms,
      'currency' => $currency,
      'display_img' => $image1_name,
      'property_description' => $prop_desc
    ];

    $insert_property = $this->createProperty($data_property);

    $request->session()->flash('Registered', 'New Hotel has been successfully registered');

    return redirect()->route('get.new-home');
  }




}
