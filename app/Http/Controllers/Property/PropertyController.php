<?php

namespace App\Http\Controllers\Property;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    /**
     * function to load the view of add property page
     * @return [View] [View of add property page]
     */
    public function getIndex() {
      return view('property.create_property');
    }
}
