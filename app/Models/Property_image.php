<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property_image extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['path', 'file_name', 'property_id'];

    /**
     * one to many relation between property and property_image
     * @return [object] instance of property
     */
    public function properties() {
      return $this->belongsTo('App\Models\Property');
    }
}
