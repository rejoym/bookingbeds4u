<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['property_contact_id', 'property_type_id', 'country_id', 'name', 'location', 'address_line', 'city', 'postal_code', 'state', 'website_url', 'no_of_rooms', 'currency', 'display_img'];

    /**
     * one to many relation between property and property_contacts
     * @return [object] instance of property_contact
     */
    public function propertyContacts() {
      return $this->belongsTo('App\Models\Property_contact');
    }

    /**
     * one to many relation between property and property_type
     * @return [object] [instance of property_type]
     */
    public function propertyTypes() {
      return $this->belongsTo('App\Models\Property_type');
    }

    /**
     * one to many relation between property and country
     * @return [object] [instance of country]
     */
    public function countries() {
      return $this->belongsTo('App\Models\Country');
    }

    /**
     * one to many relation between property and property_image
     * @return [object] [instance of property_image]
     */
    public function propertyImages() {
      return $this->hasMany('App\Models\Property_image');
    }
}
