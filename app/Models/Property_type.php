<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['label'];

    /**
     * one to many relation between property and property_type
     * @return [object] instance of property
     */
    public function properties() {
      return $this->hasMany('App\Models\Property');
    }
}
