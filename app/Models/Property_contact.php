<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property_contact extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['company', 'first_name', 'middle_name', 'last_name', 'email', 'mob_no', 'phone_region_code'];

    /**
     * one to many relation between property and property_contact model
     * @return [object] instance of property
     */
    public function properties() {
      return hasMany('App\Models\Property');
    }
}
