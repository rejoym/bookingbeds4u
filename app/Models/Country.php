<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  /**
   * arrays that are mass assignable
   * @var array
   */
    protected $fillable = ['label'];

    /**
     * one to many relation between country and property model
     * @return object instance of property
     */
    public function properties() {
      return $this->hasMany('App\Models\Properties');
    }

}
