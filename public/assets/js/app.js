$(document).ready(function() {
  //show sub menu when clicked on a parent menu item
  $('li.sorts').on('click', function(e) {
    var sub_menu = $(this).find('ul.sub-menu');
    var hide_flag = false;
    if (sub_menu.css('display') == 'block') {
      hide_flag = true;
    }
    $('ul.sub-menu').hide();
    (hide_flag) ? sub_menu.hide() : sub_menu.show();
  })
  // toggle left menu items
  $('.left-menu-icon, .short-menu').on('click', function(e) {
    e.stopPropagation();
    if (".left-menu-wrapp")
    var menu_wrapper = $('.left-menu-wrapper');
    if (menu_wrapper.css('display') == 'block') {
      menu_wrapper
        .animate({
          left: "-270px"
        }, 500, function(e) {
          menu_wrapper.hide();
        })
    }
    else {
      menu_wrapper
        .show()
        .animate({
          left: "0"
        }, 500);
    }
  })

})
