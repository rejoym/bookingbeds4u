<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_contact_id')->unsigned();
            $table->integer('property_type_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->string('name', 255);
            $table->string('location', 255);
            $table->string('address_line', 255);
            $table->string('city', 255);
            $table->integer('postal_code')->unsigned();
            $table->string('state', 255);
            $table->string('website_url', 255);
            $table->integer('no_of_rooms')->unsigned();
            $table->enum('currency', ['dollar', 'nrs', 'inrs']);
            $table->string('display_img');
            $table->text('property_description');
            $table->timestamps();
        });
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('property_contact_id')
                  ->references('id')
                  ->on('property_contacts')
                  ->onUpdate('cascade');
            $table->foreign('property_type_id')
                  ->references('id')
                  ->on('property_types')
                  ->onUpdate('cascade');
            $table->foreign('country_id')
                  ->references('id')
                  ->on('countries')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
